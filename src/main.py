from bs4 import BeautifulSoup

pass_list = ["InventoryData"]
with open("../data/inventory.xml", "r") as parse_file:
    parse_file = parse_file.read()
    soup = BeautifulSoup(parse_file, "xml")
    parse_data = soup.find("InventoryData")
    root_children = [e.name for e in parse_data.children if e.name is not None]
    for teg_name in root_children:
        print(teg_name)
        parse_teg = parse_data.find(teg_name)
        teg_children = [e.name for e in parse_teg if e.name is not None]
        for teg_name_children in teg_children:
            data_in_teg_children = parse_teg.find(teg_name_children).text
            print(f"\t{teg_name_children} - {data_in_teg_children}")
        delete_teg = soup.select_one(teg_name)
        delete_teg.decompose()

